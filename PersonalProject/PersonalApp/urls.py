from django.urls import path
from . import views

app_name = 'PersonalApp'

urlpatterns = [
    path('', views.home, name='home'),
]